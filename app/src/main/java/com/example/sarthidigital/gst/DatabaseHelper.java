package com.example.sarthidigital.gst;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by sarthidigital on 1/2/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    public static final String database_name = "gst.db";
    public static final String table_name = "gst_table";
    public static final String COL_1 = "Amount";
    public static final String COL_2 = "GST";
    public static final String COL_3 = "TotalWithGST";


    public DatabaseHelper(Context context) {
        super(context, database_name, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + table_name + " (Amount FLOAT,GST FLOAT,TotalWithGST FLOAT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + table_name);
        onCreate(db);

    }

    public boolean insertData(Float amount, Float gst, Float totalwithgst) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contantvalues=new ContentValues();
        contantvalues.put(COL_1,amount);
        contantvalues.put(COL_2,gst);
        contantvalues.put(COL_3,totalwithgst);
        long result= db.insert(table_name,null,contantvalues);
        if(result==-1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Cursor getAllData()
    {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res=db.rawQuery("select * from "+table_name,null);
        return res;
    }
}
