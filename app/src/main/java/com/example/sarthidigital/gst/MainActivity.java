package com.example.sarthidigital.gst;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    Button button0, button1, button2, button3, button4, button5, button6,
            button7, button8, button9, button10, buttonC, buttonEqual, button, button11, button12,button13;
    int count = 1;
    Float f1=0.0f,ff=0.0f,f2=0.0f;
    public static String temp="";
    EditText edt1, edt2,text;
    DatabaseHelper mydb;
    float mValueOne, mValueTwo;
    boolean a = false, b = false, c = false, d = false, e=false,f=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mydb=new DatabaseHelper(this);
        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        button10 = (Button) findViewById(R.id.button10);
        buttonC = (Button) findViewById(R.id.buttonC);
        button = (Button) findViewById(R.id.button);
        button11 = (Button) findViewById(R.id.button11);
        button12 = (Button) findViewById(R.id.button12);
        button13 = (Button) findViewById(R.id.button13);
        buttonEqual = (Button) findViewById(R.id.buttoneql);
        edt1 = (EditText) findViewById(R.id.edt1);
        edt2 = (EditText) findViewById(R.id.edt2);
        button1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (a == false) {
                            edt1.setText(edt1.getText() + "1");
                        } else {

                            if ((d == false)) {
                                edt1.setText(null);
                                d = true;
                            }
                            edt1.setText(edt1.getText() + "1");
                        }
                    }
                });

        button2.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (a == false) {
                            edt1.setText(edt1.getText() + "2");
                        } else {
                            if (d == false) {
                                edt1.setText(null);
                                d = true;
                            }
                            edt1.setText(edt1.getText() + "2");
                        }
                    }
                });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "3");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "3");

                }

            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "4");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "4");
                }
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "5");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "5");
                }
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "6");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "6");
                }
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "7");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "7");
                }
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "8");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "8");
                }
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "9");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "9");
                }

            }
        });

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == false) {
                    edt1.setText(edt1.getText() + "0");
                } else {
                    if (d == false) {
                        edt1.setText(null);
                        d = true;
                    }
                    edt1.setText(edt1.getText() + "0");
                }

            }
        });

        buttonEqual.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                          calculation();
                        try {
                            d = false;
                            f = false;
                            if (c == false) {
                                if (b == false) {
                                    mValueOne = Float.parseFloat(edt1.getText() + "");
                                    edt1.setText(null);
                                    float thr = (mValueOne * 5) / 100;
                                    mValueOne = mValueOne + thr;
                                    edt1.setText(mValueOne + "");
                                    a = true;
                                    b = true;
                                } else if (b == true) {
                                    mValueTwo = Float.parseFloat(edt1.getText() + "")/* + mValueOne*/;
                                    edt1.setText(null);
                                    float thr = (mValueTwo * 5) / 100;
                                    mValueTwo = mValueTwo + thr + mValueOne;
                                    mValueOne = mValueTwo;
                                    edt1.setText(mValueTwo + "");

                                }
                            } else {
                                a = true;
                                b = true;
                                mValueTwo = Float.parseFloat(edt1.getText() + "") + mValueOne;
                                edt1.setText(null);
                                mValueOne = mValueTwo;
                                edt1.setText(mValueTwo + "");

                            }
                        }
                        catch(Exception e)
                        {

                        }
                    }
                });


        buttonC.setOnClickListener(


                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        a = false;
                        b = false;
                        c = false;
                        d = false;
                        e = false;
                        f = false;
                        f1=0.0f;
                        f2=0.0f;
                        ff=0.0f;
                        count=1;
                        mValueOne = 0;
                        mValueTwo = 0;
                        button.setText("GST_OFF");
                        edt1.setText(null);
                    }
                });

        button10.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {

                        if (a == false) {
                            edt1.setText(edt1.getText() + ".");
                        } else {

                            if ((d == false)) {
                                edt1.setText(null);
                                d = true;
                            }
                            edt1.setText(edt1.getText() + ".");
                        }

                    }
                });

        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            System.out.println(count);
                            if (count % 2 == 0) {
                                count++;
                                c = false;
                                button.setText("GST_OFF");
                            } else {
                                count++;
                                c = true;
                                e=true;
                                button.setText("GST_ON");
                            }
                        } catch (Exception e) {
                            System.out.println("nfjfd");
                        }
                    }
                });

        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ss = "";
                String s = String.valueOf(edt1.getText());
                char[] ch = s.toCharArray();
                edt1.setText(null);
                for (int i = 0; i < ch.length - 1; i++) {
                    ss = ss + ch[i];
                }

                edt1.setText(ss);
            }
        });
        button13.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res=mydb.getAllData();
                        if(res.getCount()==0)
                        {
                            showMessage("Error","Nothing found");
                            return;
                        }

                        StringBuffer buffer=new StringBuffer();
                        while(res.moveToNext())
                        {
                            buffer.append("Amount :"+res.getFloat(0)+"\n");
                            buffer.append("GST :"+res.getFloat(1)+"\n");
                            buffer.append("TotalWithGST :"+res.getFloat(2)+"\n");

                        }
                          showMessage("Data",buffer.toString());
                    }
                });

        button12.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ff = Float.parseFloat(edt1.getText() + "");
                f2=ff-f1;
                System.out.println(f1+" "+ff+" "+f2);
                boolean isInserted =   mydb.insertData(f1,f2,ff);
                  if(isInserted==true)
                  {
                        Toast.makeText(MainActivity.this,"Data Inserted",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this,"Data Not Inserted", Toast.LENGTH_LONG).show();
                    }
            }
                });
    }


public void calculation()
{
    f1 = f1+Float.parseFloat(edt1.getText() + "");
}

    public void showMessage(String title,String Message)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }


}





